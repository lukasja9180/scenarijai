using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace S02
{
    /// <summary>
    /// Main API class.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main API method that starts the API.
        /// </summary>
        /// <param name="args">API start arguments.</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Creating and configuring API host.
        /// </summary>
        /// <param name="args">API start arguments.</param>
        /// <returns>Host builder.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
