﻿using Microsoft.EntityFrameworkCore;

namespace S02.Data
{
    /// <summary>
    /// DBContext for database actions.
    /// </summary>
    public class DatabaseContext : DbContext
    {
        /// <summary>
        /// Constructor with DBContextOptions (provides options from Startup class.
        /// </summary>
        /// <param name="options"></param>
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        /// <summary>
        /// tblStudents entity.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Maps Student entities to respectable tables.
        /// </summary>
        /// <param name="modelBuilder">Provides a simple API surface for configuring a Microsoft.EntityFrameworkCore.Metadata.IMutableModel
        //     that defines the shape of your entities, the relationships between them, and
        //     how they map to the database.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().ToTable("tblStudents");
        }
    }
}
