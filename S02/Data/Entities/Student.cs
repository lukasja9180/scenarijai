﻿using System;

namespace S02.Data
{
    /// <summary>
    /// Student entity class.
    /// </summary>
    public class Student
    {
        /// <summary>
        /// Student ID in database.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Student name.
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// Student surname.
        /// </summary>
        public string StudentSurname { get; set; }

        /// <summary>
        /// Date when student joined.
        /// </summary>
        public DateTime StudentJoinDate { get; set; }

        /// <summary>
        /// Date when student left, null if still active.
        /// </summary>
        public DateTime? StudentLeaveDate { get; set; }
    }
}
