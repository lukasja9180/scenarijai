﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using S02.Data;
using S02.Helpers;
using S02.Models;
using S02.Repository;
using S02.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static S02.Helpers.RequestTypes;

namespace S02.Controllers
{
    /// <summary>
    /// Main Controller to insert/get student data.
    /// </summary>
    [ApiController]
    [Route("api")]
    public class StudentController : ControllerBase
    {
        private readonly IStudentRepository repository;
        private readonly ILogger<StudentController> logger;

        /// <summary>
        /// Setting up controller.
        /// </summary>
        /// <param name="repository">Student repository for DB calls.</param>
        /// <param name="logger">Logger to log event into event viewer.</param>
        public StudentController(IStudentRepository repository, ILogger<StudentController> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// Method to insert data.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Insert")]
        public IActionResult Insert()
        {
            LogRequest($"{nameof(Insert)}");

            // Gets RequestModel.
            var request = GetRequest();

            // Validates request against duplicates and missing JoinDate.
            var errors = this.repository.Validate(request.Data);

            // If validation returns errors, those errors are logged into event viewer and returns Bad Request (error 400).
            if(errors.Any())
            {
                errors.ToList().ForEach(error => this.logger.LogError(1002, error));
                return BadRequest(errors);
            }

            // If validation passes, data is inserted into DB and all that information is logged into event viewer.
            this.repository.InsertData(request.Data);
            request.Data.ToList().ForEach(data => this.logger.LogInformation(1003, $"Data inserted: {data.ConvertToString()}"));

            return Ok("Inserted");
        }

        /// <summary>
        /// Method to get all data in students table.
        /// </summary>
        /// <returns>All data from tblStudents.</returns>
        [HttpGet]
        [Route("Get")]
        public IActionResult Get()
        {
            LogRequest($"{nameof(Get)}");

            var dataRetrieved = this.repository.GetData();
            dataRetrieved.ToList().ForEach(data => this.logger.LogInformation(1004, $"Data Retrieved: {data.ConvertToString()}"));

            return Ok(dataRetrieved);
        }

        /// <summary>
        /// Returns specified student from DB.
        /// </summary>
        /// <param name="id">Student ID in DB.</param>
        /// <returns>Single student information.</returns>
        [HttpGet]
        [Route("{id}/Get")]
        public IActionResult Get(Guid id)
        {
            LogRequest($"{id}/{nameof(Get)}");

            var data = this.repository.GetData(id);
            this.logger.LogInformation(1004, $"Data Retrieved: {data.ConvertToString()}");

            return Ok(data);
        }

        /// <summary>
        /// Builds RequestModel from HttpRequest request.
        /// </summary>
        /// <returns>Built request model with Student data filled in.</returns>
        private RequestModel GetRequest()
        {
            RequestModel request;
            using (var reader = new StreamReader(Request.Body))
            {
                var body = reader.ReadToEndAsync();

                request = JsonConvert.DeserializeObject<RequestModel>(body.Result);
            }

            if (request == null)
                throw new ArgumentNullException("Request body is empty or in incorrect format");

            if (request.Type != RequestType.text)
            {
                if (string.IsNullOrEmpty(request.FileLocation))
                    throw new ArgumentNullException("For json/xml type requests, parameter 'FileLocation' must be defined");

                if (request.Type == RequestType.json)
                    request.Data = JsonConvert.DeserializeObject<IEnumerable<StudentModel>>(System.IO.File.ReadAllText(request.FileLocation));
            }

            return request;
        }

        /// <summary>
        /// Logs request information - request method and requester IP.
        /// </summary>
        /// <param name="requestMethod">Method called in request.</param>
        private void LogRequest(string requestMethod)
        {
            this.logger.LogInformation(1001, $"Request '{requestMethod}' ran from: {HttpContext.Connection.RemoteIpAddress}");
        }
    }
}
