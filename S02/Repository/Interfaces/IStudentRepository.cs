﻿using S02.Models;
using System;
using System.Collections.Generic;

namespace S02.Repository.Interfaces
{
    public interface IStudentRepository
    {
        /// <summary>
        /// Inserts data to Students table.
        /// </summary>
        /// <param name="model">Model to insert.</param>
        void InsertData(IEnumerable<StudentModel> model);

        /// <summary>
        /// Gets all students as Model list.
        /// </summary>
        /// <returns>All students in DB.</returns>
        IEnumerable<StudentModel> GetData();

        /// <summary>
        /// Gets studendent by ID.
        /// </summary>
        /// <param name="id">Student id.</param>
        /// <returns>Student by ID.</returns>
        StudentModel GetData(Guid id);

        /// <summary>
        /// Validates Student Model list.
        /// </summary>
        /// <param name="modelList">List of students to validate.</param>
        /// <returns>Validation errors.</returns>
        IEnumerable<string> Validate(IEnumerable<StudentModel> modelList);

        /// <summary>
        /// Validates student data.
        /// </summary>
        /// <param name="model">Provided student to validate.</param>
        /// <returns>Validation errors.</returns>
        IEnumerable<string> Validate(StudentModel model);
    }
}