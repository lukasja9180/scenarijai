﻿using S02.Data;
using S02.Helpers;
using S02.Models;
using S02.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S02.Repository
{
    /// <summary>
    /// Repository class for calls to DB.
    /// </summary>
    public class StudentRepository : IStudentRepository
    {
        private readonly DatabaseContext ctx; 

        /// <summary>
        /// Construct repository class.
        /// </summary>
        /// <param name="ctx">Database context from model injection.</param>
        public StudentRepository(DatabaseContext ctx)
        {
            this.ctx = ctx;
        }

        /// <inheritdoc/>
        public void InsertData(IEnumerable<StudentModel> model)
        {
            var entity = model.ToEntity(true);

            ctx.AddRange(entity);

            ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public IEnumerable<StudentModel> GetData()
        {
            return ctx.Students.ToModel();
        }

        /// <inheritdoc/>
        public StudentModel GetData(Guid id)
        {
            return ctx.Students.FirstOrDefault(x => x.StudentId == id).ToModel();
        }

        /// <inheritdoc/>
        public IEnumerable<string> Validate(IEnumerable<StudentModel> modelList)
        {
            var errors = new List<string>();
            modelList.ToList().ForEach(model => errors.AddRange(Validate(model)));

            return errors;
        }

        /// <inheritdoc/>
        public IEnumerable<string> Validate(StudentModel model)
        {
            var errors = new List<string>();

            // If model is null only error returned is that model is empty.
            if (model == null)
                errors.Add("Model is empty!");
            else
            {
                // Validating if student with same name and surname already exists
                if (ctx.Students.Any(x => x.StudentName == model.Name && x.StudentSurname == model.Surname))
                    errors.Add($"Student with specified name and surname already exists! Model: {model.ConvertToString()}");

                // Validating join date, since model value cannot be null, cheking if provided value is not default value (0001-01-01)
                if (model.JoinDate == new DateTime())
                    errors.Add($"Join date is not provided! Model: {model.ConvertToString()}");
            }

            return errors;
        }
    }
}
