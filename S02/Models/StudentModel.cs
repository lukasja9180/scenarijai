﻿using System;

namespace S02.Models
{
    /// <summary>
    /// Student data retrieved/to be inserted.
    /// </summary>
    public class StudentModel
    {
        /// <summary>
        /// Student ID in database.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Student name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Student surname.
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Date when student joined.
        /// </summary>
        public DateTime JoinDate { get; set; }

        /// <summary>
        /// Date when student left, null if still active.
        /// </summary>
        public DateTime? LeaveDate { get; set; }
    }
}
