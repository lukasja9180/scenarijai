﻿using System.Collections.Generic;

namespace S02.Models
{
    /// <summary>
    /// Request model that is read from request body.
    /// </summary>
    public class RequestModel
    {
        /// <summary>
        /// Request type.
        /// </summary>
        public Helpers.RequestTypes.RequestType Type { get; set; }

        /// <summary>
        /// File location if request data is in file.
        /// </summary>
        public string FileLocation { get; set; }

        /// <summary>
        /// Request data to insert.
        /// </summary>
        public IEnumerable<StudentModel> Data { get; set; }
    }
}
