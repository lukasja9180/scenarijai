﻿namespace S02.Helpers
{
    /// <summary>
    /// Available request types.
    /// </summary>
    public static class RequestTypes
    {
        public enum RequestType
        {
            text,
            json
        }
    }
}
