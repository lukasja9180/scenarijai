﻿using S02.Data;
using S02.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S02.Helpers
{
    /// <summary>
    /// Extensions for <see cref="StudentModel"/> class objects.
    /// </summary>
    public static class ModelHelper
    {
        /// <summary>
        /// Converts <see cref="StudentModel"/> class object to entity class <see cref="Student"/> object.
        /// </summary>
        /// <param name="model">Input model object.</param>
        /// <param name="newId">Parameter to define if use existing ID (if for get or update purposes) or to create new one (for insert purposes).</param>
        /// <returns>Entity class object.</returns>
        public static Student ToEntity(this StudentModel model, bool newId = false)
        {
            if (model == null)
                return null;

            return new Student { 
                StudentId = newId ? Guid.NewGuid() : model.Id,
                StudentJoinDate = model.JoinDate,
                StudentName = model.Name,
                StudentSurname = model.Surname,
                StudentLeaveDate = model.LeaveDate
            };
        }

        /// <summary>
        /// Converts multiple <see cref="StudentModel"/> class objects to entity class <see cref="Student"/> objects.
        /// </summary>
        /// <param name="modelList">Input model objects list.</param>
        /// <param name="newId">Parameter to define if use existing ID (if for get or update purposes) or to create new one (for insert purposes).</param>
        /// <returns>List of entity class objects.</returns>
        public static IEnumerable<Student> ToEntity(this IEnumerable<StudentModel> modelList, bool newId = false)
        {
            return modelList.Select(model => model.ToEntity(newId));
        }

        /// <summary>
        /// Return all model parameters as string.
        /// </summary>
        /// <param name="model">Input model.</param>
        /// <returns>Model as string.</returns>
        public static string ConvertToString(this StudentModel model)
        {
            return $"ID: '{model.Id}'; Name: '{model.Name}'; Surname: '{model.Surname}'; JoinDate: '{model.JoinDate}'; LeaveDate: '{model.LeaveDate}'";
        }
    }
}
