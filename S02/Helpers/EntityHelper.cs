﻿using S02.Data;
using S02.Models;
using System.Collections.Generic;
using System.Linq;

namespace S02.Helpers
{
    /// <summary>
    /// Extensions for entity class <see cref="Student"/> objects. 
    /// </summary>
    public static class EntityHelper
    {
        /// <summary>
        /// Converts entity class <see cref="Student"/> object to <see cref="StudentModel"/> class object.
        /// </summary>
        /// <param name="entity">Input entity.</param>
        /// <returns>Converted entity to model.</returns>
        public static StudentModel ToModel(this Student entity)
        {
            if (entity == null)
                return null;

            return new StudentModel
            {
                Id = entity.StudentId,
                Name = entity.StudentName,
                Surname = entity.StudentSurname,
                JoinDate = entity.StudentJoinDate,
                LeaveDate = entity.StudentLeaveDate
            };
        }

        /// <summary>
        /// Converts list of entity class <see cref="Student"/> objects to list of <see cref="StudentModel"/> class objects.
        /// </summary>
        /// <param name="entityList">List of input entities.</param>
        /// <returns>List of converted enteties into models.</returns>
        public static IEnumerable<StudentModel> ToModel(this IEnumerable<Student> entityList)
        {
            return entityList.Select(entity => entity.ToModel());
        }
    }
}
